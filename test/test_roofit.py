#!/usr/bin/env python
#
#
#  IMPOSSIBLE TO FIT 2 HISTOGRAMS IN PARALLEL
#
#
#

import ROOT
from ROOT import RooRealVar, RooPolynomial, RooCategory, RooFit, RooDataSet, RooArgSet
import time
import numpy as np
# incredible!
import rootpy.stl as stl
#print("======================================== STL ")
#MapStrRootPtr = stl.map(stl.string, "TH1*")
#print("======================================== STL PAIR ")
#StrHist = stl.pair(stl.string, "TH1*")


def show(frame):
    frame.Draw()
    ROOT.gPad.Update()
    time.sleep(3)


def read_data( x ):  # argument is x varriable
    h = ROOT.TH1D("h","gaussian histogram",200,0,200)
    for i in range(10000):
        po = np.random.normal(100,6) #  0,3, 1????
        h.Fill( po )
    h2 = ROOT.TH1D("h2","gaussian histogram2",200,0,200)
    for i in range(10000):
        po = np.random.normal(105,8) #  0,3, 1????
        h2.Fill( po )
        # x axis of TH wil be x
    data1 = ROOT.RooDataHist("data","data set x",ROOT.RooArgList(x), h)
    data2 = ROOT.RooDataHist("data","data set x",ROOT.RooArgList(x), h2) 
    #data1 = ROOT.RooDataSet("data","data set x",  h,  RooArgSet(x) ) #tree
    #data2 = ROOT.RooDataSet("data","data set x",  h2, RooArgSet(x) )
    return data1,data2





    
#========== workspace is savable=================
w = ROOT.RooWorkspace("w1")
#========== factory helps with fast def.
x=ROOT.RooRealVar("x","x",0,200)
#x.setBins(100) 

mean=ROOT.RooRealVar("mean","mean",100,90,110) 
sigma=ROOT.RooRealVar("sigma","sigma",5,1,9)
# unique mean and sigma
f=ROOT.RooGaussian("f","f",x,mean,sigma) 
f2=ROOT.RooGaussian("f2","f2",x,mean,sigma) 
#// imports f,x,mean and sigma
getattr(w,'import')(f)   #w.import(f) 


data1,data2=read_data( x )
print("i... data is read")

#
# ======================== THIS WORKS================
#
##==================== ORIGINAL PART - TWO SEQUENTIAL FITS ======
frame = x.frame()
data1.plotOn(frame)
data2.plotOn(frame,ROOT.RooFit.MarkerColor(2),ROOT.RooFit.LineColor(2))

res=f.fitTo(data1,ROOT.RooFit.Save() ,ROOT.RooFit.PrintLevel(0) )
f.plotOn(frame, ROOT.RooFit.LineColor(4) ) # plot before other fit

res=f2.fitTo(data2,ROOT.RooFit.Save(),ROOT.RooFit.PrintLevel(0))
f2.plotOn(frame,ROOT.RooFit.LineColor(2))
show(frame)
print( mean  ,sigma )
########################################################################





# Declare observable
#x = RooRealVar("x","x",2.,-10,10) 
#x.setBins(40) 
## Construction a uniform pdf
#p0 = RooPolynomial("px","px",x) 
## Sample 1000 events from pdf
#data1 = p0.generate(RooArgSet(x),1000)
#data2 = p0.generate(RooArgSet(x),1000)

# Define categories
myCats = RooCategory("categories","categories")
myCats.defineType("Cat1")
myCats.defineType("Cat2")


print( RooFit.Import("Cat1",data1) )

# Make a combined dataset
#================ This is where I CRASH=============
input_hists = MapStrRootPtr()

###################### DATAHIST HAS A BIG PROBLEM
combData = ROOT.RooDataHist("combData","combined data" ,
                      ROOT.RooArgList(x) ,
                      RooFit.Index(myCats),
                      RooFit.Import("Cat1",data1) ,
                      RooFit.Import("Cat2",data2) ,
)# , RooFit::WeightVar(weighting) ) ;
#=================== DATASET MAY WORK 
# # Make a combined dataset
# combData = RooDataSet("combData","combined data" ,
#                       ROOT.RooArgSet(x) ,
#                       RooFit.Index(myCats),
#                       RooFit.Import("Cat1",data1) ,
#                       RooFit.Import("Cat2",data2) ,
# )# , RooFit::WeightVar(weighting) ) ;

print( "i... combined histograms",combData )






#x   = w.var('x')
#pdf = w.pdf('model')
print("i... WORKSPACE:")
w.Print()


simPdf = ROOT.RooSimultaneous("simPdf", "simultaneous pdf", combData)
# Associate model with the physics state and model_ctl with the control
# state
simPdf.addPdf(f, "Cat1")
simPdf.addPdf(f2, "Cat2")
# Perform a simultaneous fit
# ---------------------------------------------------
# Perform simultaneous fit of model to data and model_ctl to data_ctl
simPdf.fitTo(combData)


quit()










# Create index category and join samples
# ---------------------------------------------------------------------------
# Define category to distinguish physics and control samples events
sample = ROOT.RooCategory("sample", "sample")
sample.defineType("cat1")
sample.defineType("cat2")
# Construct combined dataset in (x,sample)
print("trying to combine data")
combData = ROOT.RooDataSet("combData","combined data",
                           ROOT.RooArgSet(x),
                           ROOT.RooFit.Index(sample),
                           ROOT.RooFit.Import("cat1",data),
                           ROOT.RooFit.Import("cat2",data2)  )
print("==============combined=========")
# Construct a simultaneous pdf in (x, sample)
# -----------------------------------------------------------------------------------
# Construct a simultaneous pdf using category sample as index
simPdf = ROOT.RooSimultaneous("simPdf", "simultaneous pdf", sample)
# Associate model with the physics state and model_ctl with the control
# state
simPdf.addPdf(f, "cat1")
simPdf.addPdf(f2, "cat2")
# Perform a simultaneous fit
# ---------------------------------------------------
# Perform simultaneous fit of model to data and model_ctl to data_ctl
simPdf.fitTo(combData)






show(frame)
quit()
