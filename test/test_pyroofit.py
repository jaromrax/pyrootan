#!/usr/bin/env python
#
#   PACKAGE PYROOFIT - I think IT IS TOO YOUNG AND SIMPL
#
#
#

import pyroofit
print("OK ======  TESTING PACKAGE PYROOFIT===>Probably too Young and too simple")

from pyroofit.models import Gauss, Chebychev
import numpy as np
import pandas as pd
import ROOT
import time

aaa=np.append(np.random.random_sample(1000)*20 + 745, np.random.normal(752, 0.7, 1000) )  
aaa=np.append(aaa,  np.random.normal(748, 0.7, 1000) )
df = {'mass':  aaa}
df = pd.DataFrame(df)

x = ROOT.RooRealVar('mass', 'M', 750, 745, 755, 'GeV')  # or x = ('mass', 745, 755)

pdf_sig =  Gauss(x, mean=(747,745, 750), sigma=(0.3, 0.1, 2), title="Signal")
pdf_sig2 = Gauss(x, mean=(753,750, 755), sigma=(0.3, 0.1, 2), title="Signal2")
pdf_bkg = Chebychev(x, n=1, title="Background")

pdf = pdf_sig +pdf_sig2 + pdf_bkg

pdf.fit(df)
pdf.plot('example_sig_bkg.pdf', legend=True)
time.sleep(3)
#if not ROOT.gROOT.IsBatch():
#    plt.show()
pdf.get()

