from pyroofit.models import Gauss, Chebychev
from pyroofit.composites import AddPdf
import numpy as np
import pandas as pd
import ROOT
import time

data=np.append(np.random.random_sample(1000)*10 + 745, np.random.normal(747, 1, 1000))
data=np.append(data, np.random.normal(754, 1, 1000))
df = {'mass': data}
df = pd.DataFrame(df)

x = ROOT.RooRealVar('mass', 'M', 750, 745, 755, 'GeV')  # or x = ('mass', 745, 755)


pdf_sig = Gauss(x, mean=(751, 755), sigma=(0.1, 1, 2), title="Signal")
pdf_sig2 =Gauss(x, mean=(745, 748), sigma=(0.1, 1, 2), title="Signal2")
pdf_bkg = Chebychev(x, n=1, title="Background")

pdf_sig=AddPdf( pdf_sig, pdf_sig2)

pdf = pdf_sig +pdf_sig2 + pdf_bkg

pdf.fit(df)
pdf.plot('example_sig_bkg.pdf', legend=True)
time.sleep(3)
pdf.get()
