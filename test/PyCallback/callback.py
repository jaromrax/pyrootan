from ROOT import TF1, TH1F, TCanvas
import time

class Linear:
    def __call__(self,x,par):
        #print(par[0]," ",par[1])
        return par[0]+x[0]*par[1]

f=TF1('pyf3', Linear(), 0.,2.,  2 ) # interval, 2 params

h=TH1F('h','test',100, 0.,2.)
f2=TF1('cf2','1.+x*1.0', 0., 2. )  # Create histogram using some function.
h.FillRandom('cf2', 1000) 
h.Fit(f)
h.Draw()

time.sleep(3)
