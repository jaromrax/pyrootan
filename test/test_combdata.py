from ROOT import RooRealVar, RooPolynomial, RooCategory, RooFit, RooDataSet, RooArgSet

# Declare observable
x = RooRealVar("x","x",2.,-10,10) 
x.setBins(40) 

# Construction a uniform pdf
p0 = RooPolynomial("px","px",x) 

# Sample 1000 events from pdf
data1 = p0.generate(RooArgSet(x),1000)
data2 = p0.generate(RooArgSet(x),1000)

# Define categories
myCats = RooCategory("categories","categories")
myCats.defineType("Cat1")
myCats.defineType("Cat2")

# Make a combined dataset
combData = RooDataSet("combData","combined data" , RooArgSet(x) ,
    RooFit.Import("Cat1",data1) ,
    RooFit.Import("Cat2",data2) ,
    RooFit.Index(myCats) )# , RooFit::WeightVar(weighting) ) ;

print( combData )
