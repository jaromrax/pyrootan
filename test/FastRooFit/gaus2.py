import ROOT
import time

#https://root.cern.ch/roofit-20-minutes
# Construct model

w = ROOT.RooWorkspace("w")
g1 = w.factory("Gaussian::g1(x[-20,20],mean[-10,10],sigma_g1[3])")
g2 = w.factory("Gaussian::g2(x,mean,sigma_g2[4,3,6])")
model = w.factory("SUM::model(frac[0.5,0,1]*g1,g2)")

x = w.var("x")
frac = w.var("frac")

# Generate 1000 events
varsForGeneration = ROOT.RooArgSet(x)
data = model.generate(varsForGeneration, 1000)

# Create likelihood function
nll = model.createNLL(data, ROOT.RooFit.NumCPU(4))

# Minimize likelihood
minimiser = ROOT.RooMinuit(nll)
minimiser.migrad()

c=ROOT.TCanvas("c") # PLOT CANVAS TO SAVE PNG
frame = x.frame()  # x,  not frac
data.plotOn(frame)
model.plotOn(frame)
frame.Draw()
c.SaveAs("gaus.png") # SAVE PNG
time.sleep(3)

# Plot likelihood scan in parameter frac
frame1 = frac.frame(ROOT.RooFit.Bins(10), ROOT.RooFit.Range(0.01,0.95))
nll.plotOn(frame1, ROOT.RooFit.ShiftToZero())

# Plot the profile likelihood in frac
profileVariables = ROOT.RooArgSet(frac)
pll_frac = nll.createProfile(profileVariables)
pll_frac.plotOn(frame1, ROOT.RooFit.LineColor(ROOT.kRed))

frame1.Draw()
ROOT.gPad.Update()
time.sleep(3)
