import ROOT
#import rootpy.stl as stl

#stl.generate("std::map<std::string, TH1*>", "map;string;TH1.h")
#stl.generate("std::pair<std::string, TH1*>", "map;string;TH1.h")
#stl.generate("std::map<std::string, RooDataHist*>", "map;string;RooDataHist.h")
#stl.generate("std::pair<std::string, RooDataHist*>", "map;string;RooDataHist.h")
ROOT.gInterpreter.GenerateDictionary("std::map<std::string, TH1*>", "map;string;TH1.h")
ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, TH1*>", "map;string;TH1.h")
ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataHist*>", "map;string;RooDataHist.h")
ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataHist*>", "map;string;RooDataHist.h")

# <codecell>

histo1 = ROOT.TH1F("histo1", "histo1", 1000, 0, 10)
histo2 = ROOT.TH1F("histo2", "histo2", 1000, 0, 10)
histo1.Fill(3)
histo1.Fill(3)
histo1.Fill(2)
histo2.Fill(5)
histo2.Fill(6)
histo2.Fill(7)
histo2.Fill(7)
histo1.SetLineColor(ROOT.kRed)
histo2.SetLineColor(ROOT.kGreen)
histo1.Draw()
histo2.Draw("same")

# <codecell>

x = ROOT.RooRealVar("x", "x", 0, 10)
#x.setBins(10)
data1 = ROOT.RooDataHist("data1", "data1", ROOT.RooArgList(x), histo1)
data2 = ROOT.RooDataHist("data2", "data2", ROOT.RooArgList(x), histo2)
cat = ROOT.RooCategory("category", "category")
cat.defineType('cat1')
cat.defineType('cat2')

# <codecell>

roohist1 = ROOT.RooHist(histo1)
roohist2 = ROOT.RooHist(histo2)
roohist1.SetLineColor(ROOT.kRed)
roohist2.SetLineColor(ROOT.kGreen)
frame0 = x.frame()
frame0.addPlotable(roohist1)
frame0.addPlotable(roohist2)
frame0.Draw()

# <codecell>

frame1 = x.frame()
data1.plotOn(frame1, ROOT.RooFit.LineColor(ROOT.kRed))
data2.plotOn(frame1, ROOT.RooFit.LineColor(ROOT.kGreen))
frame1.Draw()

# <codecell>

m1 = ROOT.std.map('string, TH1*')()
m1.insert(ROOT.std.pair('string, TH1*')('cat1', histo1))
m1.insert(ROOT.std.pair('string, TH1*')('cat2', histo2))

m2 = ROOT.std.map('string, RooDataHist*')()
m2.insert(ROOT.std.pair('string, RooDataHist*')('cat1', data1))
m2.insert(ROOT.std.pair('string, RooDataHist*')('cat2', data2))

# <codecell>

simul_data1 = ROOT.RooDataHist("data1", "data1", ROOT.RooArgList(x), cat, m1)
simul_data2 = ROOT.RooDataHist("data2", "data2", ROOT.RooArgList(x), cat, m2)

# <codecell>

frame2 = x.frame()
simul_data1.plotOn(frame2, ROOT.RooFit.Cut("category==category::cat1"), ROOT.RooFit.LineColor(ROOT.kRed))
simul_data1.plotOn(frame2, ROOT.RooFit.Cut("category==category::cat2"), ROOT.RooFit.LineColor(ROOT.kGreen))
frame2.Draw()

# <codecell>

frame3 = x.frame()
simul_data2.plotOn(frame3, ROOT.RooFit.Cut("category==category::cat1"), ROOT.RooFit.LineColor(ROOT.kRed))
simul_data2.plotOn(frame3, ROOT.RooFit.Cut("category==category::cat2"), ROOT.RooFit.LineColor(ROOT.kGreen))
frame3.Draw()

# <codecell>

data1_bis = simul_data1.reduce(ROOT.RooFit.Cut("category==category::cat1"))
data2_bis = simul_data1.reduce(ROOT.RooFit.Cut("category==category::cat2"))
frame4 = x.frame()
data1_bis.plotOn(frame4, ROOT.RooFit.LineColor(ROOT.kRed))
data2_bis.plotOn(frame4, ROOT.RooFit.LineColor(ROOT.kGreen))
frame4.Draw()

# <codecell>

data1_tris = simul_data2.reduce(ROOT.RooFit.Cut("category==category::cat1"))
data2_tris = simul_data2.reduce(ROOT.RooFit.Cut("category==category::cat2"))
frame5 = x.frame()
data1_tris.plotOn(frame5, ROOT.RooFit.LineColor(ROOT.kRed))
data2_tris.plotOn(frame5, ROOT.RooFit.LineColor(ROOT.kGreen))
frame5.Draw()
