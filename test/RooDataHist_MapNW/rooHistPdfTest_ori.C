//Dear emacs this is c++

//Std includes
#include <map>

//Root includes
#include "TH1.h"

//RooFit includes
#include "RooRealVar.h"
#include "RooCategory.h"
#include "RooDataHist.h"

//---------------------------------------------------------------------------
// main part of code here
void rooHistPdfTest(){

  //Histograms that where supposed to be imported from elsewhere
  TH1D *histA = new TH1D("histA", "histA", 10, -1.0, 1.0);
  TH1D *histB = new TH1D("histB", "histB", 10, -1.0, 1.0);
  TH1D *histC = new TH1D("histC", "histC", 10, -1.0, 1.0);

  //Observable definitions
  RooRealVar a("a", "a", -1.0, 1.0);
  RooRealVar b("b", "b", -1.0, 1.0);
  RooRealVar c("c", "c", -1.0, 1.0);

  //Define categorys here
  RooCategory channelCat("channelCat", "channelCat");
  channelCat.defineType("channelA");
  channelCat.defineType("channelB");
  channelCat.defineType("channelC");

  //Contain observables in a RooArgSet
  RooArgSet obs( a, b, c );

  //Import histograms into RooDataHist
  map<string, RooDataHist*> datasets;
  datasets["channelA"] = new RooDataHist("dataA", "dataA", RooArgList(*obs.find("a")), histA);
  datasets["channelB"] = new RooDataHist("dataB", "dataB", RooArgList(*obs.find("b")), histB);
  datasets["channelC"] = new RooDataHist("dataC", "dataC", RooArgList(*obs.find("c")), histC);
  
  RooDataHist *combData = new RooDataHist("combData", "combData", obs, channelCat, datasets);
  combData->Print("v");

} // EOF

