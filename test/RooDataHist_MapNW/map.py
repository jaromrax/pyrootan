import ROOT
import rootpy.stl as stl
#import rootpy.stl.generate as generate

#@attr('slow')
def test_stl():
    stl.generate('map<int,vector<float> >', '<vector>;<map>')
    stl.generate('map<int,vector<int> >', '<vector>;<map>')
    stl.generate('vector<TLorentzVector>', '<vector>;TLorentzVector.h')
 
    ROOT.std.map('int,vector<float>')
    ROOT.std.map('int,vector<int>')
    ROOT.std.vector('TLorentzVector')
 
    temp = stl.CPPType.from_string('vector<vector<vector<int> > >')
    temp.ensure_built()
 
    stl.vector('vector<map<int, string> >')
    stl.vector(stl.string)()
    stl.vector('string')()
    stl.vector(int)
 
    stl.map("string", "string")
    stl.map(stl.string, stl.string)
    stl.map(int, stl.string)
    stl.map(stl.string, int)
    stl.map("string", ROOT.TLorentzVector)
 
    histmap = stl.map("string", ROOT.TH1D)()
    a = ROOT.TH1D("a", "a", 10, -1, 1)
    histmap["a"] = a
 
    StrHist = stl.pair(stl.string, "TH1*")
 
    stl.generate('pair<map<string,TH1*>::iterator,bool>', '<map>;<TH1.h>')
    histptrmap = stl.map(stl.string, "TH1*")()
    histptrmap.insert(StrHist("test", a))
 
    assert histptrmap["test"] is a


test_stl()

