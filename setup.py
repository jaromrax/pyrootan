#!/usr/bin/env python
import os
from setuptools import setup, find_packages
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="Pyrootan",
    description="Automatically created environment for python package",
    url="http://",
    author=" me ",
    author_email="me@example.com",
    licence="",
    version='0.0.1-dev0',
    packages=find_packages(),
    packagedata={'pyrootan': ['data/*']},
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    scripts = ['bin/pyrootan'],
    install_requires = ['numpy==1.16.2'],
)
#
#   To Access Data in Python: :
#   DATA_PATH = pkg_resources.resource_filename('pyrootan', 'data/')
#   DB_FILE =   pkg_resources.resource_filename('pyrootan', 'data/file')
