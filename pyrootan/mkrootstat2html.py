#!/usr/bin/python3
#######################################
#  pip3 install nbformat
#
# ONEDIR ===  ./looktree.py -d ./ -o 20150606_3he26mg_root  
#
#  look into a root directory     -d
#  convert :  look into each directory in -d
#             look at each root file [make_html_root_hists]
#             get info from histograms [analyze_root_file]
#             create html, ipynb
#
#######################################
#import ROOT
#from ROOT import TFile, TTree

import ROOT as root
import numpy as np

import argparse
import glob

import os
import datetime
# notebook conver
import nbformat as nbf
import subprocess



# class HistogramNotFoundError(KeyError):
#      pass

# def get_histogram(self, name):
#     hist = self.file.Get(name)
#     if not hist:
#         raise HistogramNotFoundError(name)
#     return hist




###################################################
#
#       s001  s002  de6  e6
#       RETURN -------- all in list
#
###################################################
def analyze_root_file( file ):
    
    f = root.TFile( file )
    myTree = f.Get("nanot")
    entries=myTree.GetEntries()
    print("D... nanot entries=", entries )
    for i in myTree:
        start=i.time
        break
    if entries!=0:
        # ROOT TIME 672768317.892317  to  UNIX
        start=start+788918400 # convert to unix
        DD=datetime.datetime.fromtimestamp( start ).strftime('%Y/%m/%d %H:%M:%S %a')
        #print(DD)
        #   print(entries)
    else:
        DD="unknown"
    s001 = f.Get("s001")
    s001_mean=s001.GetMean()
    s001_n=s001.GetEntries()
    s001_total=s001_mean * s001_n   # total counts in SCALER
    #    print( s001_mean , s001_n , s001_total )
    
    s002 = f.Get("s002")  # charge
    s002_mean=s002.GetMean()
    s002_n=s002.GetEntries()
    s002_total=s002_mean * s002_n      # total counts in SCALER
    #s002_total=s002_mean * s002_n/10/1000   # total counts in SCALER
    #    print( s002_mean , s002_n , s002_total,"mC" )
    
    thist = f.Get("Thist")  # charge
    thist_title=thist.GetTitle()
    seconds=float( thist_title.split()[2].strip() ) 
    #    print( thist_title, "=",seconds )
    
    s004 = f.Get("s004")  # charge
    s004_mean=s004.GetMean()
    s004_n=s004.GetEntries()
    s004_total=s004_mean * s004_n   # total counts in SCALER
    #    print( s004_mean , s004_n , s004_total," gene" , s004_total/seconds, "cps")

    de6 = f.Get("c022")  # charge
    de6_n=de6.GetEntries()
    e6 = f.Get("c006")  # charge
    e6_n=e6.GetEntries()
    # print( s004_mean , s004_n , s004_total," gene" , s004_total/seconds, "cps")

    #  get the list of keys in directory ==========
    k=f.GetListOfKeys()
    kentries=k.GetEntries()
    histos=0
    for n in range( kentries ):
        name=k.At(n).GetName()
        if name!="nanot":
            histos=histos+1
    
    return entries, s001_total, s002_total, s004_total, seconds, DD, de6_n, e6_n, histos








def decode_LILIST( LL , desc ,frstdirs="."): # LiList , descripton{runname,material}
    """
    input: List of lists
    main list == table ;   internal list = row

    LL is resorted !   desc  must be dictionary


    returns tr td td tr  TABLE
    """
    #print("==============+++++>",desc)
    TBL=""
    
    for i in sorted(LL):  # Sort by 1st column =========== DATE START
        txt=""
        col=0
        material="" # each row has a new material==color
        for j in i:
            col=col+1
            #print("-------> /{}/".format(j) )
            if j in desc.keys():
                material=desc[j]

                
            #print("=================== MATERIAL", material,"///",j)
            style='style="color:black"'
            if material.find("c12")>=0:    style='style="color:black;font-weight:bold;"'
            if material.find("c13")>=0:    style='style="color:silver;font-weight:bold;"'
            if material.find("c14")>=0:    style='style="color:gray;font-weight:bold;"'
            
            if material.find("mylar")>=0:  style='style="color:red;font-weight:normal;"'
            
            if material.find("mg")>=0:     style='style="color:magenta;font-weight:bold;"'
            if material.find("mg26")>=0:   style='style="color:magenta;font-weight:bold;"'
            
            if material.find("cu")>=0:   style='style="color:#b87333;font-weight:bold;"'
            
            if material.find("o16")>=0:    style='style="color:blue;font-weight:bold;"'
            if material.find("ne22")>=0:    style='style="color:navy;font-weight:bold;"'
            if material.find("air")>=0:    style='style="color:cyan;font-weight:bold;"'
            if material.find("alpha")>=0:  style='style="color:green;font-weight:bold;"'

            
            #############################################
            # for each column, we can have an xtra change of style.  style2=....
            #
            if col<2: # date
                if j.find("Sun")>0: # Red vacations
                    style2='style="'+'background-color:pink;'+style.split('style="')[1]
                elif j.find("Sat")>0:
                    style2='style="'+'background-color:MistyRose;'+style.split('style="')[1]
                else:
                    style2=style
                txt=txt+'<td align="left" '+style2+'> '+j+' </td>'
                
            elif col<3: # duration
                txt=txt+'<td align="right" style="color:blue"> '+j+' </td>'
                
            elif col<4: # filename
                # a href, no blue underscore;  trick- put style to a href
                style2='style="'+'text-decoration:none;'+style.split('style="')[1]
                txt=txt+'<td align="left" > <a '+style2+' href="http://147.231.102.62/WEBDATA/'+frstdirs+'/'+j+'"> '+j+' </a></td>'
                #txt=txt+'<td align="left" '+style+'> '+j+' </td>'
                
            elif col==5 or col==6: # 7==Q  #  brown column
                style2='style="'+'background-color:OldLace;'+style.split('style="')[1]
                txt=txt+'<td align="right" '+style2+'> '+j+' </td>'
                
            else:   # normal,colored,right
                txt=txt+'<td align="right" '+style+'> '+j+' </td>'

                
            #txt=txt # compose ONE TABLE ROW
        txt=txt+'<td '+style+'>'+material+'</td>'
        #====== all j in i (i is one row of table)
        #print(txt, end="")
        #print( material )
        TBL=TBL+"<tr bgcolor=\"white\">"+txt+"</tr>\n"
            
    print(TBL)
    return TBL









        

def make_html_root_hists(dir, filename):  # in specific directory
    """
    gets initial directory and  directory name(==filename) with root files
    creates HTML with all root files.
    """

    
    PATH=os.path.expanduser(args.diveinto)
    if PATH[-1]!="/": PATH=PATH+"/"
    #============== print HTML ============
    if dir=="": # always !
        newfile=PATH+filename+".html"
        descfile=PATH+filename+"/.description"
    # else:
    #     newfile=dir+"/"+filename+".html"
    #     descfile=filename+"/.description"
    #     ##descfile=dir+"/"+".description"

    ####### read .description ##########
    #try:
    print( "D...... opening ..... ", descfile  )
    runs_mat={}  # material of runs
    try:
        with open( descfile ,"r") as f:
            desc=f.readlines()
            desc=[i.rstrip() for i in desc]
            print( desc )
            # i want to create directory d={}
            runs_mat={ i.split()[0]:" ".join(i.split()[1:])  for i in desc if len(i.split())>1 }
            print(runs_mat,"-----------------------------------")
    except:
        print("D... NO .description FILE ...............")


        
    ################ now ... single rows == root files ############
    print("D... seearching in ", PATH+filename+'/RUN*.root')
    roots=list( sorted( glob.glob( PATH+filename+'/RUN*.root', recursive=True) ) )
    root2=list( sorted( glob.glob( PATH+filename+'/run*.root', recursive=True) ) )
    roots=roots+root2
    
    if roots==[]:return 1


    
    #==============  create HTML =============    
    print("i... making root hists in",dir,"newfile=",newfile)
    with open(newfile,"w") as f:
        f.write("<html>\n<style>\n.imgContainer{\n float:left;\n}table { border-collapse: collapse;}table, th, td { border: 1px solid black; padding:5px;}</style>\n<body>\n")
        ############################
        #
        #    <th>Tree Mevts</th>
        #     <th>SCA1 Mevts</th>
        #
        tablea="""
        <table style="width=100%">  
<tr>
        
    <th>Start </th>
    <th>duration</th>
    <th>name</th> 
    <th>Gate_rate</th>
    <th> Q [uC]</th>
    <th> I [nA]</th>
    <th>gene cps</th>

    <th>dE6 cps</th>
    <th>E6 cps</th>

    <th># histo</th>
</tr>
"""
        f.write(tablea)
        # THE CORRECT WAY HERE IS TO : 1/create a list of lists
        #                              2/ sort it and create the table
        LList=[]
        for i in roots:
            print("D...                    analyzing root file=",i)
            print("D...")
            res=analyze_root_file(i) #========== THIS IS THE MAIN OTO TO SEE ROOTFILE
            #
            # return entries, s001_total, s002_total, s004_total, seconds, DD, de6_n, e6_n, histos
            #         0           1         2           3            4      5   6       7      8
            basename=os.path.basename(i)
            #f.write("<tr>\n")
            txt=""
            inlist=[]
            # STARTED /DURATION/  NAME
            inlist.append( "{}".format(res[5]) )
            inlist.append( "{:14}".format( str(datetime.timedelta(seconds= round(res[4]))  )) ) # 1d 4:30:21

            inlist.append( "{:30s}".format(basename).strip() ) # root filename
            
            #inlist.append( "{:.3f}".format(res[0]/1000000) )  # Tree milion events
            #inlist.append( "{:.3f}".format(res[1]/1000000) )  # MEGA GATES    
             
            inlist.append( "{:.1f}".format(res[2]/res[4]) ) # gate rate == cps
            
            inlist.append( "{:7.1f}".format(res[1]/1000/10) )   # Q
            inlist.append( "{:5.0f}".format(res[1]*1000/1000/10/res[4]) )   # I  [nC/s = nA]

            inlist.append( "{:7.2f}".format(res[3]/res[4]) )     # gene 
            # de6 e6
            inlist.append( "{:7.2f}".format(res[6]/res[4]) )  #de6 cps
            inlist.append( "{:7.2f}".format(res[7]/res[4]) )
            #  n histograms (non nastro)
            inlist.append( "{:4.0f}".format(res[8])  )
            LList.append( inlist )
            txt=decode_LILIST(LList , runs_mat , filename) # runs_mat{runnuber, material}
        f.write( txt )

        ##################################### Finish the table ####
        f.write("</table>\n")
        f.write("\n</body>\n</html>\n")
        return 0




        
    


        
def html_to_notebook(dir, filename):

    PATH=os.path.expanduser(args.diveinto)
    if PATH[-1]!="/": PATH=PATH+"/"

    if dir=="":
        newfile=PATH+filename+".ipynb"
    #else:
    #    newfile=dir+"/"+filename+".ipynb"
    print("i... converting",newfile,"  in",dir)
    nb = nbf.v4.new_notebook()
    fname=newfile
    text='from IPython.display import HTML\nHTML("'+filename+'.html")'
    #nb['cells'] = [nbf.v4.new_markdown_cell(text)]
    nb['cells'] = [nbf.v4.new_code_cell(text)]
    with open(fname, 'w') as f:
        nbf.write(nb, f)
        
    print('i... compile with jupyter')
    CMD="jupyter nbconvert --execute --inplace "+fname
    #p=subprocess.check_call( CMD , shell=True)
    p=subprocess.call( CMD.split() )





    

##########################################################
#         MAIN
##########################################################
file="./20170602_dp26mg_root/run48_20170604_023204.root"
#file="./20170602_dp26mg_root/run23_20170531_115027.root"

parser=argparse.ArgumentParser(description="""
 ... 
""")

parser.add_argument('-d','--diveinto', default="", help='', required=True)

parser.add_argument('-o','--onedir', default="", help='', required=False)
#parser.add_argument('-a','--allconvert', action='store_true',default=False, help='convert all')
args=parser.parse_args()


#================== look into  directory (-ies)
if args.onedir!="":
    frstdirs=[args.onedir]
else:
    #  /*/  ... first level directories
    #  /**/ ... all levels
    frstdirs=glob.glob( os.path.expanduser(args.diveinto)+'/*/', recursive=True) # ** means all subdirs
frstdirs=[ i.strip("/") for i in frstdirs]
print("globbibg result:",frstdirs)

if args.diveinto[-1]!="/": args.diveinto=args.diveinto+"/"

for i in frstdirs:
    print("D...  making ROOT HTML from DIRECTORY ",i)
    res=make_html_root_hists("", i) # make localy
    #break
    if res==0:html_to_notebook( "", i )


    

#for entry in myTree:         
#    # Now you have acess to the leaves/branches of each entry in the tree, e.g.
#    events = entry.time

     
