#!/usr/bin/env python
#https://www.nikhef.nl/~vcroft/GettingStartedWithRooFit.html

# this is something:
#
#https://www.nikhef.nl/~vcroft/RooFit.html
#https://www.nikhef.nl/~vcroft/RooStats.html


import ROOT
import time
#import rootnotes
#c1=rootnotes.default_canvas()
#http://theoryandpractice.org/2014/03/roofit-statistical-modeling-language-in-ipython-notebook/#.XQz59sszbkk



w = ROOT.RooWorkspace()
w.factory('Gaussian::g(x[-5,5],mu[-3,3],sigma[1])')
w.factory('Exponential::e(x,tau[-.5,-3,0])')
w.factory('SUM::model(s[50,0,100]*g,b[100,0,1000]*e)')
w.Print()


x = w.var('x')
pdf = w.pdf('model')
frame = x.frame()
data = pdf.generate(ROOT.RooArgSet(x))
data.plotOn(frame)
fitResult = pdf.fitTo(data,ROOT.RooFit.Save(),ROOT.RooFit.PrintLevel(-1))
pdf.plotOn(frame)
frame.Draw()
ROOT.gPad.Update()
#c1
print("i...      FAST FIT ")
time.sleep(3)



x = ROOT.RooRealVar("x","x",-10,10)
mean = ROOT.RooRealVar("mean","Mean of Gaussian",-10,10)
sigma = ROOT.RooRealVar("sigma","Width of Gaussian",3,-10,10)

gauss = ROOT.RooGaussian("gauss","gauss(x,mean,sigma)",x,mean,sigma)



xframe = x.frame()
gauss.plotOn(xframe)
xframe.Draw()
ROOT.gPad.Update()
#c1
print("i...      LOOK AT THE MODEL ")

time.sleep(3)


newframe = x.frame()
gauss.plotOn(newframe)
sigma.setVal(2)
gauss.plotOn(newframe,ROOT.RooFit.LineColor(2))
newframe.Draw()
ROOT.gPad.Update()

print("i...      ANOTHER PARAMETERS SAME MODEL ")

time.sleep(3)






h1 = ROOT.TH1D("h1","gaussian histogram",20,-10,10)
h1.FillRandom("gaus",10000)
h2 = ROOT.TH1D("h2","gaussian histogram",20,-10,10)
for i in range(10000): h2.Fill(ROOT.gRandom.Gaus(0,3))

x = ROOT.RooRealVar("x","x",-10,10)
l = ROOT.RooArgList(x)
data = ROOT.RooDataHist("data", "data set with x1", l, h1)
data2 = ROOT.RooDataHist("data2", "data set with x2", l, h2)

#For some reason, the RooDataHist wont take the RooRealVar as an argument. As such we add it to the RooArgList.

xframe = x.frame()
data.plotOn(xframe)
data2.plotOn(xframe,ROOT.RooFit.MarkerColor(2))
xframe.Draw()
h1.Draw("same")
ROOT.gPad.Update()
print("i...      DATA PLOTTED  ")
time.sleep(3)

#c1






import numpy as np
tree = ROOT.TTree("tree","tree")
x = np.zeros(1,dtype=float)
tree.Branch("x",x,'x/D')
for i in range(10000):
    x[0] = np.random.normal(0,3,1)
    tree.Fill()

    
x = ROOT.RooRealVar("x","x",-10,10)
data = ROOT.RooDataSet("data","dataset from tree",tree,ROOT.RooArgSet(x))

xframe = x.frame()
data.plotOn(xframe,ROOT.RooFit.Binning(25))
xframe.Draw()
#c1
ROOT.gPad.Update()
print("i...     DATA TO TREE _ AND PLOTTED FROM ROODATADSET  ")

time.sleep(3)






x = ROOT.RooRealVar("x","x",-10,10)
mean = ROOT.RooRealVar("mean","Mean of Gaussian",0,-10,10)
sigma = ROOT.RooRealVar("sigma","Width of Gaussian",3,-10,10)
gauss = ROOT.RooGaussian("gauss","gauss(x,mean,sigma)",x,mean,sigma)

data = gauss.generate(ROOT.RooArgSet(x),10000)

xframe = x.frame()
data.plotOn(xframe, ROOT.RooLinkedList())
gauss.plotOn(xframe)
xframe.Draw()
ROOT.gPad.Update()
print("i...     FITTING MODEL TO DATA  ")
time.sleep(3)

#c1
result = gauss.fitTo(data,ROOT.RooFit.PrintLevel(-1))
mean.setVal(0.42)

mean.Print()
sigma.Print()
mean.setConstant(ROOT.kTRUE)
sigma.setConstant(ROOT.kFALSE)

sigma.setRange(0.1,3)
result = gauss.fitTo(data,ROOT.RooFit.Minos(ROOT.kTRUE),ROOT.RooFit.PrintLevel(-1))

gauss.fitTo(data,ROOT.RooFit.Range(-5,5),ROOT.RooFit.PrintLevel(-1))

xframe = x.frame()
data.plotOn(xframe,ROOT.RooLinkedList())
gauss.plotOn(xframe)
xframe.Draw()
ROOT.gPad.Update()
print("i...     FITTING MODEL TO DATA - RESTRICTED PARAMETERS  ")
time.sleep(3)

#c1



intGaussX = gauss.createIntegral(ROOT.RooArgSet(x))
intGaussX.getVal()

cdf = gauss.createCdf(ROOT.RooArgSet(x))
xframe = x.frame()
cdf.plotOn(xframe)
xframe.Draw()
ROOT.gPad.Update()
print("i...      CALCULATED INTEGRAL ")
time.sleep(3)

#c1
